//Requires y Cargamos la configuracion "config.json"
var config_json = require('./config.json');
var spawn = require('child_process').spawn;
var ws = require('ws');

//Cargamos la coniguracion en variables globales
var STREAM_SECRET = config_json.secret,
    CTRL_WEBSOCKET_PORT = config_json.control_server_port || 8090,
    STREAM_MAGIC_BYTES = 'jsmp'; // Must be 4 bytes

// Servidor Websocket
var controlSocketServer = new ws.Server({port: CTRL_WEBSOCKET_PORT});

controlSocketServer.on("connection", function(socket) {
   // this.process = null;
    var killed = false;

    console.log( '[controlSocketServer.onConnection] New WebSocket Connection ('+controlSocketServer.clients.length+' total)' );

    socket.on('close', function(code, message){
        console.log( '[controlSocketServer.onConnection] Disconnected WebSocket ('+controlSocketServer.clients.length+' total)' );
    });

    socket.on("message", function(data, flags) {
        var data_j = JSON.parse(data);

        if (data_j.cmd === "start_videostream"){
            //requrl arma la cadena: http://127.0.0.1:8045/1234/640/480/
            var requrl= "http://" + config_json.local_host +
                        ":" + config_json.stream_video_server_http_port +
                        "/" + config_json.secret +
                        "/"  + config_json.video_width +
                        "/" + config_json.video_height +
                        "/";

            var cmd = 'ffmpeg';
            var res = config_json.video_width + 'x' + config_json.video_height;

            var args =  [   '-framerate','30' ,
                '-s', res,
                '-f', 'avfoundation',
                '-pix_fmt', 'bgr0',
                '-i', '0',
                '-f', 'mpeg1video',
               // '-vf','"crop=iw-mod(iw\,2):ih-mod(ih\,2)"',
                '-b', '1000k',
                requrl];

            if (config_json.os === "linux"){
                args[5] = "video4linux2";
                args[9] = "/dev/video0"
            }


            console.log("[controlSocketServer.onMessage] 'start_videostream' received, ready to run :");
            this.process = spawn(cmd, args);
            killed = false;

            console.log(cmd + ' ' + args.join(" ") + ' with PID:' + this.process.pid);

            this.process.on('close', function (code,signal) {
                console.log('[controlSocketServer.process.onClose] ffmpeg process terminated with code ' + code);
                killed = true;
            });
        }

        if (data_j.cmd === "stop_videostream"){
            console.log(this.process.pid);
            console.log('stop_videostream killed: ' + killed);
            if (! killed) {
                console.log("[controlSocketServer.onMessage] 'stop_videostream' received, sending [q] to ffmpeg to quit...");
                this.process.stdin.write('q');
                //this.process.kill('SIGHUP');
            }
        }

        if (data_j.cmd === "echo"){
            console.log("[controlSocketServer.onMessage] 'echo' received , echoing msg...")

            socket.send(data_j.msg);
        }


        if (data_j.cmd === "send_coord"){
            console.log("send_coord recivido")
        }

    });
});


console.log('[controlSocketServer]  Server started host: ' + config_json.remote_host + " port: " + CTRL_WEBSOCKET_PORT);
