//Cargamos la configuracion "config.json"
var config_json = require('./config.json');

//Cargamos la coniguracion en variables globales
var STREAM_SECRET = config_json.secret,
	STREAM_PORT = config_json.stream_video_server_http_port || 8082,
	WEBSOCKET_PORT = config_json.stream_video_server_ws_port || 8084,
	STREAM_MAGIC_BYTES = 'jsmp'; // Must be 4 bytes

var width = config_json.video_width,
	height = config_json.video_height;

// Servidor Websocket
var ws = require('ws');
var wsSocketServer = new ws.Server({port: WEBSOCKET_PORT});

wsSocketServer.on("connection", function(socket) {
	var streamHeader = new Buffer(8);
	streamHeader.write(STREAM_MAGIC_BYTES);
	streamHeader.writeUInt16BE(width, 4);
	streamHeader.writeUInt16BE(height, 6);
	socket.send(streamHeader, {binary:true});

	console.log( '[wsSocketServer.onConnection] New WebSocket Connection ('+wsSocketServer.clients.length+' total)' );
	console.log('buffer: ' + width + ' ' + height);

	socket.on('close', function(code, message){
		console.log( '[wsSocketServer.onClose] Disconnected WebSocket ('+wsSocketServer.clients.length+' total)' );
	});
});


wsSocketServer.broadcast = function(data, opts) {
	for( var i in this.clients ) {
		if (this.clients[i].readyState == 1) {
			this.clients[i].send(data, opts);
		}
		else {
			console.log( 'Error: Client ('+i+') not connected.' );
		}
	}
};

// Servidor Video Stream Http
var httpStreamServer = require('http').createServer( function(request, response) {
	var params = request.url.substr(1).split('/');

	if( params[0] == STREAM_SECRET ) {
		width = (params[1] || 320)|0;
		height = (params[2] || 240)|0;

		console.log(
			'[httpStreamServer.onRequest] Stream Connected: ' + request.socket.remoteAddress +
			':' + request.socket.remotePort + ' size: ' + width + 'x' + height
		);

		request.on('data', function(data){
			wsSocketServer.broadcast(data, {binary:true});
		});
	}
	else {
		console.log(
			'[httpStreamServer.onRequest] Failed Stream Connection: '+ request.socket.remoteAddress +
			request.socket.remotePort + ' - wrong secret.'
		);
		response.end();
	}
}).listen(STREAM_PORT);

//httpStreamServer.listen(STREAM_PORT, function(){
//	console.log("[httpStreamServer.listen] http server listening for Mpeg stream in " + config_json.local_host + ":" + STREAM_PORT + "/<secret>/<width>/<height>");
//});

console.log('[wsSocketServer] Awaiting WebSocket connections on ws://' + config_json.local_host + ':'+ WEBSOCKET_PORT+'/');