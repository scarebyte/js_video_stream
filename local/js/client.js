//Cargamos la configuracion "config.json"
var config_json = require('./config.json');
//var WebSocket = require('ws');

//Stream Client
var stream_client = new WebSocket( 'ws:'+config_json.local_host+':' + config_json.stream_video_server_ws_port );
var canvas = document.getElementById('videoCanvas');
var player = new jsmpeg(stream_client, {canvas:canvas});

//Control Client
var control_client = new WebSocket( 'ws:'+config_json.remote_host+':' + config_json.control_server_port);
control_client.onmessage = function(e) {
    console.log("[onmessage]" + e.data);
};

//El canvas es 400x400
function draw() {
        var xo=0;
        var yo=0;
        var backh=150;
        var backw=200;


        var canvas = new fabric.Canvas('jcanvas');

        var rect_up_back = new fabric.Rect({
            left: 0,
            top: 0,
            fill: 'rgb(27,142,210)',
            width: backw,
            height: backh
        });

        var rect_down_back = new fabric.Rect({
            left: 0,
            top: backh,
            fill: 'rgb(46, 22, 18)',
            width: backw,
            height: backh
        });

        var rect1 = new fabric.Rect({
          left: 0,
          top: 100,
          fill: 'rgb(256, 256, 256)',
          width: 100,
         height: 2
        });

        canvas.add(rect_up_back);
        canvas.add(rect_down_back);
        canvas.add(rect1);

}

$(window).load(function() {
    draw();

    $("#send-button").click(function() {
        var json_query = {"cmd":"start_videostream"};
        control_client.send(JSON.stringify(json_query));
        console.log("clicked start");
    });

    $("#kill-button").click(function() {
        var json_query = {"cmd":"stop_videostream"};
        control_client.send(JSON.stringify(json_query));
        console.log("clicked stop");
    });

    $("#echo-button").click(function() {
        var json_query = {"cmd":"echo","msg":"hola ahi"};
        control_client.send(JSON.stringify(json_query));
        console.log("clicked echo");
    });

    $("#send-coord-button").click(function() {
        var json_query = {"cmd":"send_coord"};
        control_client.send(JSON.stringify(json_query));
    });
});
